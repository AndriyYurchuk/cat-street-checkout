import classNames from 'classnames';
import styles from './Button.module.scss';

export const ButtonVariants = {
  CLEAR: 'clear',
  PRIMARY: 'primary',
  ICON: 'icon',
};

export const Button = ({
  children,
  className,
  variant = ButtonVariants.CLEAR,
  ...props
}) => {
  const mods = {
    [styles[variant]]: true,
  };
  return (
    <button className={classNames(styles.Button, mods, className)} {...props}>
      <div className={styles.buttonContent}>{children}</div>
    </button>
  );
};
