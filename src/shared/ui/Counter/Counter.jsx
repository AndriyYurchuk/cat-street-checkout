import classNames from 'classnames';
import { memo, useEffect } from 'react';
import { Button, ButtonVariants } from '@/shared/ui/Button/Button';
import styles from './Counter.module.scss';

export const Counter = memo(({ className, value, setValue }) => {
  const addOne = () => {
    setValue(value + 1);
  };

  const minusOne = () => {
    if (value > 0) {
      setValue(value - 1);
    }
  };

  return (
    <div className={classNames(styles.Counter, className)}>
      <Button
        className={styles.control}
        variant={ButtonVariants.CLEAR}
        onClick={minusOne}
      >
        -
      </Button>
      <div className={styles.quantity}>{value}</div>
      <Button
        className={styles.control}
        variant={ButtonVariants.CLEAR}
        onClick={addOne}
      >
        +
      </Button>
    </div>
  );
});
