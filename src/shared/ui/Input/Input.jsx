import classNames from 'classnames';
import styles from './Input.module.scss';

export const Input = ({
  className,
  id,
  label,
  value,
  onChange,
  type = 'text',
  ...props
}) => {
  const onChangeHandler = (e) => {
    onChange?.(e.target.value);
  };
  return (
    <div className={classNames(styles.Input, className)}>
      <input
        id={id}
        type={type}
        value={value}
        className={styles.inputField}
        onChange={onChangeHandler}
        placeholder=" "
        autoComplete="off"
        {...props}
      />
      <label className={styles.inputLabel}>{label}</label>
    </div>
  );
};
