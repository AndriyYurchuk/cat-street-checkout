import classNames from 'classnames';
import styles from './Chip.module.scss';
import { ReactComponent as CloseSvg } from '@/shared/assets/images/close.svg';
import { Button } from '../Button/Button';
import { memo } from 'react';

export const Chip = memo(({ className, text, onDelete }) => {
  const handleDelete = () => {
    onDelete(text);
  };
  return (
    <div className={classNames(styles.Chip, className)}>
      {text}{' '}
      <Button className={styles.deleteChip} onClick={handleDelete}>
        <CloseSvg />
      </Button>
    </div>
  );
});
