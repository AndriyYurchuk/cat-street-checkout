import React, { useCallback, useEffect, useRef, useState } from 'react';
import classNames from 'classnames';
import { Portal } from '@/shared/ui/Portal/Portal';
import { ReactComponent as CloseSvg } from '@/shared/assets/images/close.svg';
import { Button, ButtonVariants } from '@/shared/ui/Button/Button';
import styles from './Sidebar.module.scss';

const ANIMATION_DELAY = 300;

export const Sidebar = ({ className, children, isOpen, lazy, onClose }) => {
  const [isClosing, setIsClosing] = useState(false);
  const [isMounted, setIsMounted] = useState(false);
  const timerRef = useRef();

  const onContentClick = (e) => {
    e.stopPropagation();
  };

  const handleClose = useCallback(() => {
    if (onClose) {
      setIsClosing(true);
      timerRef.current = setTimeout(() => {
        onClose();
        setIsClosing(false);
      }, ANIMATION_DELAY);
    }
  }, [onClose]);

  useEffect(() => {
    if (isOpen) {
      setIsMounted(true);
    }

    return () => {
      clearTimeout(timerRef.current);
    };
  }, [isOpen]);

  const mods = {
    [styles.opened]: isOpen,
    [styles.isClosing]: isClosing,
  };

  if (lazy && !isMounted) {
    return null;
  }

  return (
    <Portal>
      <div className={classNames(styles.Sidebar, mods, className)}>
        <div className={styles.overlay} onClick={handleClose}>
          <div className={styles.content} onClick={onContentClick}>
            <Button
              variant={ButtonVariants.ICON}
              className={styles.closeButton}
              onClick={handleClose}
            >
              <CloseSvg />
            </Button>
            {children}
          </div>
        </div>
      </div>
    </Portal>
  );
};
