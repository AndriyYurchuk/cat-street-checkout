import styles from './Logo.module.scss';
import { ReactComponent as LogoSvg } from '@/shared/assets/images/catstreet-logo.svg';
export const Logo = () => {
  return <LogoSvg className={styles.Logo} />;
};
