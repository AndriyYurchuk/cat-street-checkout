import { useCallback, useState } from 'react';
import { Button, ButtonVariants } from '@/shared/ui/Button/Button';
import { Sidebar } from '@/shared/ui/Sidebar/Sidebar';
import { Logo } from '@/shared/ui/Logo/Logo';
import { ReactComponent as CartSvg } from '@/shared/assets/images/cart.svg';
import { Cart } from '@/components/Cart/Cart';
import { useStore } from '@/shared/hooks/useStore';

const HomePage = () => {
  const { initDefault } = useStore();
  const [openSidebar, setOpenSidebar] = useState(false);

  const onOpenSidebar = useCallback(() => {
    setOpenSidebar(true);
  }, []);

  const onCloseSidebar = useCallback(() => {
    setOpenSidebar(false);
  }, []);

  return (
    <div className="page">
      <Logo className="logo" />
      <Button
        className="card-button"
        variant={ButtonVariants.ICON}
        onClick={onOpenSidebar}
      >
        <CartSvg />
      </Button>

      <Sidebar isOpen={openSidebar} onClose={onCloseSidebar}>
        <Cart />
      </Sidebar>
      <h2>Home page</h2>
      <Button onClick={initDefault}>Init default values</Button>
    </div>
  );
};

export default HomePage;
