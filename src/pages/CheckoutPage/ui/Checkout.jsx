import { CheckoutInfo } from '@/components/CheckoutInfo/ui/CheckoutInfo';
import { Order } from '@/components/Order';
import { Button, ButtonVariants } from '@/shared/ui/Button/Button';
import { Logo } from '@/shared/ui/Logo/Logo';
import styles from './Checkout.module.scss';

const Checkout = () => {
  return (
    <div className="page" id="checkout-page">
      <Logo className="logo" />
      <h1 className="page-title">Checkout</h1>

      <div className={styles.content}>
        <CheckoutInfo />
        <div className={styles.orderBlock}>
          <h2>Your Order</h2>
          <Order className={styles.orderDetails} />
          <Button className={styles.orderPay} variant={ButtonVariants.PRIMARY}>
            Pay now
          </Button>
        </div>
      </div>
    </div>
  );
};

export default Checkout;
