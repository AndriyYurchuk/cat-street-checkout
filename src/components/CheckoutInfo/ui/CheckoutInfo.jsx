import classNames from 'classnames';
import styles from './CheckoutInfo.module.scss';
import { DeliveryDetails } from './DeliveryDetails/DeliveryDetails';
import { PaymentDetails } from './PaymentDetails/PaymentDetails';
import { YourDetails } from './YourDetails/YourDetails';

export const CheckoutInfo = ({ className }) => {
  return (
    <div className={classNames(styles.CheckoutInfo, className)}>
      <YourDetails />
      <DeliveryDetails />
      <PaymentDetails />
    </div>
  );
};
