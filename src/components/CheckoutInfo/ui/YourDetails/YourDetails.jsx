import { Input } from '@/shared/ui/Input/Input';
import classNames from 'classnames';
import styles from './YourDetails.module.scss';

export const YourDetails = ({ className }) => {
  return (
    <div className={classNames(styles.YourDetails, className)}>
      <h2>Your Details</h2>
      <form className={styles.form}>
        <div className={classNames(styles.email, styles.formItem)}>
          <Input label={'your email'} className={styles.input} />
        </div>
        <div className={classNames(styles.phone, styles.formItem)}>
          <Input label={'mobile phone'} className={styles.input} />
        </div>
        <div className={classNames(styles.description, styles.formItem)}>
          Your phone number is required for delivery & shipping updates.
        </div>
        <div className={classNames(styles.fName, styles.formItem)}>
          <Input label={'first name'} className={styles.input} />
        </div>
        <div className={classNames(styles.lName, styles.formItem)}>
          <Input label={'last name'} className={styles.input} />
        </div>
      </form>
    </div>
  );
};
