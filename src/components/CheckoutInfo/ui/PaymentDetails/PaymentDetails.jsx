import classNames from 'classnames';
import styles from './PaymentDetails.module.scss';

export const PaymentDetails = ({ className }) => {
  return (
    <div className={classNames(styles.PaymentDetails, className)}>
      <h2>Payment Details</h2>
    </div>
  );
};
