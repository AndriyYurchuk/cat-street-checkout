import classNames from 'classnames';
import { Input } from '@/shared/ui/Input/Input';
import styles from './DeliveryDetails.module.scss';

export const DeliveryDetails = ({ className }) => {
  return (
    <div className={classNames(styles.DeliveryDetails, className)}>
      <h2>Delivery Details</h2>
      <form>
        <Input label={'country'} className={styles.input} />
        <Input label={'delivery address'} className={styles.input} />

        <div className={styles.radioField}>
          <div className={styles.option}>
            <input type={'radio'} name="shipping" id="free" />
            <label htmlFor="free">Free Shipping</label>
          </div>
          <div className={styles.price}>$25.30</div>
        </div>
        <div className={styles.radioField}>
          <div className={styles.option}>
            <input type={'radio'} name="shipping" id="standard" />
            <label htmlFor="free">Standard Shipping</label>
          </div>
          <div className={styles.price}>$25.30</div>
        </div>
        <div className={styles.radioField}>
          <div className={styles.option}>
            <input type={'radio'} name="shipping" id="express" />
            <label htmlFor="free">Express Shipping</label>
          </div>
          <div className={styles.price}>$25.30</div>
        </div>
      </form>
    </div>
  );
};
