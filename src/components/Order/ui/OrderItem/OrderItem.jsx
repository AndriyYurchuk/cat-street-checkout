import { memo, useEffect, useState } from 'react';
import classNames from 'classnames';
import { Counter } from '@/shared/ui/Counter/Counter';
import styles from './OrderItem.module.scss';

export const OrderItem = memo(({ className, data, changeItemQuantity }) => {
  const { name, price, quantity, img } = data;
  const [value, setValue] = useState(quantity);

  useEffect(() => {
    changeItemQuantity(data, value);
  }, [value]);

  const imageStyle = {
    backgroundImage: `url(${img})`,
  };

  return (
    <div className={classNames(styles.OrderItem, className)}>
      <div className={styles.photo} style={imageStyle}>
        {!img && 'no photo'}
      </div>
      <div className={styles.info}>
        <div className={styles.name}>{name}</div>
        <div className={styles.bottom}>
          <Counter value={value} setValue={setValue} />
          <div className={styles.price}>${price}</div>
        </div>
      </div>
    </div>
  );
});
