import classNames from 'classnames';
import { memo } from 'react';
import styles from './OrderTotal.module.scss';

export const OrderTotal = memo(({ className, total }) => {
  return (
    <div className={classNames(styles.OrderTotal, className)}>
      <div className={styles.item}>
        <div className={styles.option}>Subtotal</div>
        <div className={styles.value}>${total}</div>
      </div>
      <div className={styles.item}>
        <div className={styles.option}>Shipping</div>
        <div className={styles.value}>calculated next step</div>
      </div>
      <div className={styles.item}>
        <div className={styles.option}>Discounts</div>
        <div className={styles.value}>-$22.00</div>
      </div>
      <div className={styles.item}>
        <div className={styles.bold}>TOTAL</div>
        <div className={styles.priceBlock}>
          <div className={styles.currency}>AUD</div>
          <div className={styles.bold}>${total}</div>
        </div>
      </div>
    </div>
  );
});
