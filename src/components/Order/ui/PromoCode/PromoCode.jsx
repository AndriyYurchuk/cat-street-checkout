import { Button } from '@/shared/ui/Button/Button';
import { Chip } from '@/shared/ui/Chip/Chip';
import classNames from 'classnames';
import { memo, useEffect, useState } from 'react';
import styles from './PromoCode.module.scss';

export const PromoCode = memo(
  ({ className, promoCodes, addPromoCode, deletePromoCode }) => {
    const [inputValue, setInputValue] = useState('');
    const [isEdit, setIsEdit] = useState(promoCodes.length > 0 ? true : false);

    const enableEditMode = () => {
      setIsEdit(true);
    };

    const handleChangeInput = (e) => {
      setInputValue(e.target.value);
    };

    const onAdd = () => {
      if (inputValue === '') {
        return;
      }
      addPromoCode(inputValue);
      setInputValue('');
    };

    const onDelete = (code) => {
      deletePromoCode(code);
    };

    useEffect(() => {
      if (promoCodes.length === 0) setIsEdit(false);
    }, [promoCodes]);

    return (
      <div className={classNames(styles.PromoCode, className)}>
        {isEdit ? (
          <div className={styles.editableBlock}>
            <div className={styles.controls}>
              <input
                type="text"
                className={styles.input}
                value={inputValue}
                onChange={handleChangeInput}
                placeholder="Coupon Code"
              />
              <Button className={styles.addCouponButton} onClick={onAdd}>
                APPLY
              </Button>
            </div>
            <div className={styles.chipsBlock}>
              {promoCodes.map((promoCode) => {
                return (
                  <Chip key={promoCode} text={promoCode} onDelete={onDelete} />
                );
              })}
            </div>
          </div>
        ) : (
          <div className={styles.enterCodeBlock}>
            Promo Code?{' '}
            <Button className={styles.button} onClick={enableEditMode}>
              Enter Code
            </Button>
          </div>
        )}
      </div>
    );
  }
);
