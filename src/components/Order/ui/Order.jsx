import classNames from 'classnames';
import { useStore } from '@/shared/hooks/useStore';
import styles from './Order.module.scss';
import { OrderList } from './OrderList/OrderList';
import { OrderTotal } from './OrderTotal/OrderTotal';
import { PromoCode } from './PromoCode/PromoCode';

export const Order = ({ className }) => {
  const {
    totalPrice,
    products,
    promoCodes,
    changeItemQuantity,
    addPromoCode,
    deletePromoCode,
  } = useStore();

  return (
    <div className={classNames(styles.Order, className)}>
      {products.length === 0 ? (
        <div className={styles.emptyCart}>
          There’s nothing for your poor cat in your cart!
        </div>
      ) : (
        <>
          <div className={styles.top}>
            <OrderList
              items={products}
              changeItemQuantity={changeItemQuantity}
            />
            <PromoCode
              promoCodes={promoCodes}
              addPromoCode={addPromoCode}
              deletePromoCode={deletePromoCode}
            />
          </div>
          <div className={styles.footer}>
            <OrderTotal total={totalPrice} />
          </div>
        </>
      )}
    </div>
  );
};
