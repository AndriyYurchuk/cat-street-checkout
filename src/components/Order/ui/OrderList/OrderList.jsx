import { memo } from 'react';
import classNames from 'classnames';
import { OrderItem } from '../OrderItem/OrderItem';
import styles from './OrderList.module.scss';

export const OrderList = memo(({ className, items, changeItemQuantity }) => {
  return (
    <div className={classNames(styles.OrderList, className)}>
      {items.map((productItem) => {
        return (
          <OrderItem
            key={productItem.id}
            data={productItem}
            changeItemQuantity={changeItemQuantity}
          />
        );
      })}
    </div>
  );
});
