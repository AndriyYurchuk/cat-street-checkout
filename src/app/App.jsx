import { Suspense } from 'react';
import { AppRouter } from './providers/router';

function App() {
  return (
    <div className="App">
      <Suspense fallback="">
        <AppRouter />
      </Suspense>
    </div>
  );
}

export default App;
