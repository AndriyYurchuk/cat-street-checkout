import { CheckoutPage } from '@/pages/CheckoutPage';
import { HomePage } from '@/pages/HomePage';

export const routeConfig = [
  {
    path: '/',
    element: <HomePage />,
  },
  {
    path: '/checkout',
    element: <CheckoutPage />,
  },
];
