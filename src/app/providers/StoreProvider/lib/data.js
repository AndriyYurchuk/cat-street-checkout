import img1 from '@/shared/assets/images/cat-street-creme_2000.webp';
import img2 from '@/shared/assets/images/cat-street-olive_2000.webp';

export const productList = [
  {
    id: 1,
    name: 'Bouclé Bungalow “Creme” Cover',
    price: 239.0,
    quantity: 1,
    img: img1,
  },
  {
    id: 2,
    name: 'Replacement Cover in “Catnip”',
    price: 129.0,
    quantity: 1,
    img: img2,
  },
];
