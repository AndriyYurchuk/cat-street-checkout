import { useLocalStorage } from '@/shared/hooks/useLocalStorage';
import { useEffect, useState } from 'react';
import { productList } from '../lib/data';
import { StoreContext } from '../lib/StoreContext';

const countTotalPrice = (products) => {
  let sum = 0;
  products.forEach((e) => {
    sum += e.quantity * e.price;
  });
  return sum;
};

export const StoreProvider = ({ children }) => {
  const [products, setProducts] = useLocalStorage('cart_products', productList);
  const [promoCodes, setPromoCodes] = useLocalStorage('promo_codes', []);
  const [totalPrice, setTotalPrice] = useState(countTotalPrice(products));

  const changeItemQuantity = (item, newValue) => {
    if (newValue === 0) {
      deleteProduct(item.id);
      return;
    }
    const index = products.findIndex((e) => e.id === item.id);
    const newProducts = [...products];
    newProducts[index].quantity = newValue;

    setProducts([...newProducts]);
  };

  const initDefault = () => {
    setProducts(productList);
  };

  const deleteProduct = (id) => {
    const filteredProducts = [...products].filter((e) => e.id !== id);
    setProducts([...filteredProducts]);
  };

  const addPromoCode = (code) => {
    setPromoCodes([...promoCodes, code]);
  };

  const deletePromoCode = (codeToDelete) => {
    const filteredPromoCodes = [...promoCodes].filter(
      (e) => e !== codeToDelete
    );
    setPromoCodes([...filteredPromoCodes]);
  };

  useEffect(() => {
    setTotalPrice(countTotalPrice(products));
  }, [products]);

  const props = {
    products,
    promoCodes,
    totalPrice,
    changeItemQuantity,
    addPromoCode,
    deletePromoCode,
    initDefault,
  };
  return (
    <StoreContext.Provider value={props}>{children}</StoreContext.Provider>
  );
};
